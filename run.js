#!/usr/bin/env node

var fs = require('fs');
var request = require('request');
var rimraf = require('rimraf');
var AdmZip = require('adm-zip');
var readDir = require('readdir');

var mavenMetaUrl = 'https://maven.atlassian.com/content/groups/public/com/atlassian/aui/aui-flat-pack/maven-metadata.xml';
request(mavenMetaUrl,function(err, resp, body){
  var latestVer = body.match(/<latest>([\s\S]*?)<\/latest>/i)[1];
  var latestVerUrl = "https://maven.atlassian.com/content/groups/public/com/atlassian/aui/aui-flat-pack/" +
    latestVer + "/aui-flat-pack-" + latestVer + ".zip";
  request(latestVerUrl,{encoding:null},function(err, resp, body){
    var zip = new AdmZip(body);
    rimraf('./aui',function(err){
      if(!err){
        zip.extractAllTo('./',true);
        var main = []
          .concat(readDir.readSync('./aui/aui/css').map(function(e){return ["./aui/aui/css", e].join('/')}))
          .concat(readDir.readSync('./aui/aui/js').map(function(e){return ["./aui/aui/js", e].join('/')}));
        var json = {
          "name": "aui",
          "version": latestVer,
          "main": main
        };
        fs.writeFileSync('./component.json',JSON.stringify(json, null, "  "))
      }
    });

  });
});